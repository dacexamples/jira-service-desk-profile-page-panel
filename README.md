# JIRA Service Desk Profile Page Panel add-on example.

Welcome to this add-on example.

This add-on example shows you how you can add a panel to the JIRA Service Desk profile page.

We are using the ACE ([Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express)) framework. 

## Want to know more about adding a panel in the JIRA Service Desk profile page?

[Adding your Panel to the JIRA Service Desk profile page](https://developer.atlassian.com/jiracloud/jira-service-desk-modules-customer-portal-39988271.html#JIRAServiceDeskmodules-Customerportal-Profilepagepanel).